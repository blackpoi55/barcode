import React from 'react';
import QRCode from "react-qr-code";
export const qrcode = (value) => {
    let res = []
    if (value.type == "qrcodenum") {
        let tranfer = value.exam.substring(0, value.exam.length - value.last.length);
        console.log(value)
        console.log(value.last.length)
        let dataloop = []
        for (let index = parseInt(value.first); index <= parseInt(value.last); index++) {
            let data = index
            while (data.toString().length < value.last.length) {
                data = "0" + data.toString()
            }
            data = data.toString()
            data = tranfer + data
            dataloop.push(data)
        }
        console.log(dataloop)
        let arr = dataloop
        let page = [];
        let index = 0
        let count = []
        if (arr) {
            let mod = Math.ceil(arr.length / 24)
            for (let x = 0; x < mod; x++) {
                for (let i = 0; i < 24; i++) {
                    if (arr[index]) {
                        count.push(arr[index])
                    }
                    index++
                }
                page.push(count)
                count = []
            }
            console.log(page)
            // setslicepage(page)
        }

        let data = page
        console.log(data)
        if (data) {
            for (let index1 = 0; index1 < data.length; index1++) {
                res.push(
                    <div className='section grid grid-cols-4 gap-4'>
                        {data[index1].map((p, index) => (
                            <>
                                <QRCode size={100} className='m-5' value={p} />
                                {/* <QRCode
                                size={256}
                                style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                                value={value}
                                viewBox={`0 0 256 256`}
                            /> */}
                            </>
                        ))}
                    </div >
                )
            }
        }
    }
    else{
        res = <QRCode className='m-5' value={value.exam} />
    }
    console.log(res)
    return (res)
}