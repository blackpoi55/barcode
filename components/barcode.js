import ReactJSBarcode from 'react-jsbarcode';
export const barcode = (value) => {
    let res = []
    if (value.type == "barcodenum") {
        let tranfer = value.exam.substring(0, value.exam.length - value.last.length);
        console.log(value)
        console.log(value.last.length)
        let dataloop = []
        for (let index = parseInt(value.first); index <= parseInt(value.last); index++) {
            let data = index
            while (data.toString().length < value.last.length) {
                data = "0" + data.toString()
            }
            data = data.toString()
            data = tranfer + data
            dataloop.push(data)
        }
        console.log(dataloop)
        let arr = dataloop
        let page = [];
        let index = 0
        let count = []
        if (arr) {
            let mod = Math.ceil(arr.length / 24)
            for (let x = 0; x < mod; x++) {
                for (let i = 0; i < 24; i++) {
                    if (arr[index]) {
                        count.push(arr[index])
                    }
                    index++
                }
                page.push(count)
                count = []
            }
            console.log(page)
            // setslicepage(page)
        }
        let data = page
        console.log(data)
        if (data) {
            for (let index1 = 0; index1 < data.length; index1++) {
                res.push(
                    <div className='section grid grid-cols-4 gap-4'>
                        {data[index1].map((p, index) => (
                            <>
                                <ReactJSBarcode className='w-h' value={p} options={{ format: 'code128' }} renderer="svg" />
                            </>
                        ))}
                    </div >
                )
            }
        }
    }
    else{
        res =<ReactJSBarcode className='w-full' value={value.exam} options={{ format: 'code128' }} renderer="svg" />
    }
    console.log(res)
    return (res)
}