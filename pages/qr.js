import { useState, useRef, useEffect } from 'react';
import Quagga from 'quagga';

export default function Scanner() {
  const [scanResult, setScanResult] = useState('');
  const videoRef = useRef(null);

  useEffect(() => {
    return () => {
      Quagga.stop(); // หยุด Quagga เมื่อ component unmount
    };
  }, []);

  const startScan = () => {
    Quagga.init({
      inputStream: {
        name: "Live",
        type: "LiveStream",
        target: videoRef.current, // ใช้ ref จากตัวองค์ประกอบ video
        constraints: {
          facingMode: "environment",
        },
      },
      decoder: {
        readers: ["code_128_reader", "qr_code_reader"], // สามารถเพิ่ม readers อื่นๆ ตามที่ต้องการ
      },
      locate: true,
    }, (err) => {
      if (err) {
        console.error('Error initializing Quagga:', err);
        return;
      }
      console.log("Quagga has been initialized");
      Quagga.start();
    });

    Quagga.onDetected((result) => {
      console.log('Detected code:', result);
      setScanResult(result.codeResult.code);
      alert(`Scan result: ${result.codeResult.code}`);
      Quagga.stop(); // หยุด Quagga หลังจากจับได้ครั้งแรก
    });
  };

  return (
    <div>
      <button onClick={startScan}>Start Scan</button>
      <div ref={videoRef} style={{ width: '100%' }}></div>
      {scanResult && <p>Scan result: {scanResult}</p>}
    </div>
  );
}
