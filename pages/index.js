import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState } from 'react';
import ReactJSBarcode from 'react-jsbarcode';
import { useRouter } from 'next/router'
export default function Home() {
  const Router = useRouter();
  const [dis, setdis] = useState("disabled")
  const [value, setvalue] = useState({
    first: "",
    last: "",
    exam: ""
  })
  const sendBarcodeclick = async () => {
    if (value.first && value.last && value.exam) {
      Router.push(`/pdf?first=${value.first}&last=${value.last}&exam=${value.exam}&type=${"barcodenum"}`)
    }
    else{
      Router.push(`/pdf?first=${value.first}&last=${value.last}&exam=${value.exam}&type=${"barcodeone"}`)
    }
  }
  const sendQRclick = async () => {
    if (value.first && value.last && value.exam) {
      Router.push(`/pdf?first=${value.first}&last=${value.last}&exam=${value.exam}&type=${"qrcodenum"}`)
    }
    else{
      Router.push(`/pdf?first=${value.first}&last=${value.last}&exam=${value.exam}&type=${"qrcodeone"}`)
    }
  }
  return (
    <div className="min-w-screen min-h-screen bg-gray-900 flex items-center justify-center px-5 py-5">
      <div className="bg-gray-100 text-gray-500 rounded-3xl shadow-xl w-full overflow-hidden">
        <div className="md:flex w-full h-full">
          <div className=" md:block w-1/2 bg-indigo-500 py-10 px-10 text-white flex justify-center items-center text-center center">
            <Image width={300} height={300}
              src="/82253830_1857377594395302_6559702726547079168_n.jpg"
              alt="profile"
              className="mx-auto border-8 bdgreencolor"
            />
          </div>
          <div className="w-full md:w-1/2 py-10 px-5 md:px-10">
            <div className="text-center mb-10">
              <h1 className="font-bold text-3xl text-gray-900">Barcode</h1>
              <p>สร้าง บาร์โค้ด</p>
            </div>
            <div>
              <div className="flex -mx-3">
                <div className="w-1/2 px-3 mb-5">
                  <label for="" className="text-xs font-semibold px-1">เริ่มตั้งแต่</label>
                  <div className="flex">
                    <div className="w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center"><i className="mdi mdi-account-outline text-gray-400 text-lg"></i></div>
                    <input onChange={(e) => setvalue({ ...value, first: e.target.value })} type="text" className="w-full -ml-10 pl-10 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500" placeholder="1" />
                  </div>
                </div>
                <div className="w-1/2 px-3 mb-5">
                  <label for="" className="text-xs font-semibold px-1">จนถึง (ไม่ควรเกิน 950)</label>
                  <div className="flex">
                    <div className="w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center"><i className="mdi mdi-account-outline text-gray-400 text-lg"></i></div>
                    <input onChange={(e) => setvalue({ ...value, last: e.target.value })} type="text" className="w-full -ml-10 pl-10 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500" placeholder="950" />
                  </div>
                </div>
              </div>
              <div className="flex -mx-3">
                <div className="w-full px-3 mb-5">
                  <label for="" className="text-xs font-semibold px-1">ตัวอย่างbarcodeจริง</label>
                  <div className="flex">
                    <div className="w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center"><i className="mdi mdi-email-outline text-gray-400 text-lg"></i></div>
                    <input type="email" onChange={(e) => setvalue({ ...value, exam: e.target.value })} className="w-full -ml-10 pl-10 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500" placeholder="A99990000001" />
                  </div>
                </div>
              </div>
              <div className='flex flex-row'>
                  <div className="w-full px-3 mb-5">
                    <button onClick={() => sendQRclick()} className="block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-3 py-3 font-semibold">QRcode</button>
                  </div>
                  <div className="w-full px-3 mb-5">
                    <button onClick={() => sendBarcodeclick()} className="block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-3 py-3 font-semibold">Barcode</button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
