import { useState, useRef, useEffect } from 'react';
import jsQR from 'jsqr';

export default function Scanner() {
  const [scanResult, setScanResult] = useState('');
  const videoRef = useRef(null);
  const activeStream = useRef(null);

  useEffect(() => {
    // ทำความสะอาดเมื่อคอมโพเนนต์ถูก unmount
    return () => {
      if (activeStream.current) {
        activeStream.current.getTracks().forEach(track => track.stop());
      }
    };
  }, []);

  const startScan = () => {
    navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })
      .then(function(stream) {
        activeStream.current = stream;
        videoRef.current.srcObject = stream;
        videoRef.current.play();
        scanQRCode();
      })
      .catch(function(err) {
        console.error('Error accessing the camera', err);
      });
  };

  const scanQRCode = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
  
    const scan = () => {
      if (!videoRef.current || videoRef.current.readyState !== videoRef.current.HAVE_ENOUGH_DATA) {
        console.log('Waiting for video to be ready or videoRef is not available.');
        requestAnimationFrame(scan);
        return;
      }
  
      canvas.height = videoRef.current.videoHeight;
      canvas.width = videoRef.current.videoWidth;
      ctx.drawImage(videoRef.current, 0, 0, canvas.width, canvas.height);
      const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      const code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: 'dontInvert',
      });
  
      if (code) {
        setScanResult(code.data);
        alert(`Scan result: ${code.data}`); // แสดงผลลัพธ์ด้วย alert
        activeStream.current.getTracks().forEach(track => track.stop());
      } else {
        console.log('No QR code detected. Scanning again...');
        requestAnimationFrame(scan);
      }
    };
  
    scan();
  };

  return (
    <div>
      <button onClick={startScan}>Start Scan</button>
      <video ref={videoRef} style={{ width: '100%' }}></video>
      {scanResult && <p>Scan result: {scanResult}</p>}
    </div>
  );
}
