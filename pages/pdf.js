import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState } from 'react';
import ReactJSBarcode from 'react-jsbarcode';
import { barcode } from '../components/barcode';
import { qrcode } from '../components/qrcode';
import { useRouter } from 'next/router'
export default function pdf() {
    const Router = useRouter();
    const [arr, setarr] = useState()
    const [slicepage, setslicepage] = useState()
    useEffect(() => {
        let value = {
            first: Router.query.first,
            last: Router.query.last,
            exam: Router.query.exam,
            type: Router.query.type
        }
        console.log(Router.query,value)
        let data = ""
        if (Router.query.type == "barcodenum"||Router.query.type == "barcodeone") {
            data = barcode(value)
        }
        else if (Router.query.type == "qrcodenum"||Router.query.type == "qrcodeone") {
            data = qrcode(value)
        }
        // console.log(data)
        setarr(data)
        // window.print()
    }, [])
    return (
        <div className='h-full w-full'>
            <div className='a4'>
                {arr}
            </div>
        </div>
    )
}
